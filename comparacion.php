<html>
	<head>
		<title>Ejemplo de operadores de Comparacion</title>
	</head>
<body>
<h1>Ejemplo de comparacion en php</h1>
<?php
$a = 8;
$b = 3;
$c= 3;
echo $a == $b, " <br>";
echo $a != $b, "<br>";
echo $a < $b, " <br>";
echo $a > $b, " <br>";
echo $a >= $c, " <br>";
echo $a <= $c, " <br>";

/* 
==  variable a igual a variable b: 0 (falso)
!=  variable a no es igual a variable b: 1 (verdadero)
<  	variable a menor que variable b: 0 (falso) 
>	variable a mayor que variable b: 1 (verdadero)
>=	variable a mayor o igual que variable b: 1 	(verdadero) 
<=	variable a menor o igual que variable b: 0( falso) 
*/

?>
</body>
</html>
